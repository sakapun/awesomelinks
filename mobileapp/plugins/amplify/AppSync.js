﻿import {APPSYNC_API_KEY} from "react-native-dotenv"

export default {
  graphqlEndpoint:
    'https://v4k7tansijgx3ngkxflfwflmdq.appsync-api.ap-northeast-1.amazonaws.com/graphql',
  region: 'ap-northeast-1',
  authenticationType: 'API_KEY',
  apiKey: APPSYNC_API_KEY
}
