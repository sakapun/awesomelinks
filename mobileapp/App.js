import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

import './plugins/amplify'
import { API, graphqlOperation } from 'aws-amplify'

const query = `query Q($limit: Int, $nextToken: String){
        querySort(limit: $limit, nextToken: $nextToken) {
          items {
            mergedAt
            url
            title
            description
          },
          nextToken
        }
      }`


export default class App extends React.Component {
  state = {
    items: []
  }

  componentWillMount() {
    API.graphql(graphqlOperation(query, {limit: 10})).then(data => {
      this.setState({
        items: data.data.querySort.items
      })
    })
  }

  render() {
    return (
      <View style={styles.container}>
        {this.state.items.map((item, index) => {
          return <Text key={index}>{item.title}</Text>
        })}
        <Text>Open App.js to start working on your app!</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
