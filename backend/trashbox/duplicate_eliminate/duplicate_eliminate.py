from model.AwesomeVueLinks import AwesomeVueLinks
from bs4 import BeautifulSoup as bs
import re
import pandas as pd

def all_dump_to_csv():
    fcsv = open("duplicate.csv", mode="w")
    fcsv.write("Title,Url,Description,MergedAt\n")

    count = 0
    for link in AwesomeVueLinks.scan():
        fcsv.write("\"{}\",{},\"{}\",{}\n".format(link.Title, link.Url, link.Description, link.MergedAt))
        count += 1


    fcsv.close()

    print(count)

# dynamodbをダンプ
# all_dump_to_csv()


def create_delete_list():
    all_results = pd.read_csv("duplicate.csv")
    # print(all_results.sort_values("Title"))

    duplicated_checks = all_results.duplicated(subset=["Title", "Url"], keep=False)
    duplicated_list = all_results[duplicated_checks]

    delete_checks = duplicated_list.sort_values(["Title", "Description"]).duplicated(subset=["Title"], keep="first")
    delete_list = duplicated_list[delete_checks]
    return delete_list


# csvに消すリストを出力した
# create_delete_list().to_csv("duplicate_then_delete_list.csv")


for _, row in create_delete_list().iterrows():
    link = AwesomeVueLinks.get("link", row.MergedAt)
    print(link.delete())

