from model.AwesomeVueLinks import AwesomeVueLinks
from bs4 import BeautifulSoup as bs
import re

html = ""
with open("awesome-vue-github.html") as f:
    html = bs(f.read(), features="html.parser")

ferror = open("error-log-nonlink.csv", mode="w")
ferror.write("Title,Url,Msg\n")

for link in AwesomeVueLinks.scan(AwesomeVueLinks.Url.does_not_exist()):
    try:
        liHref = html.find(["strong", "a"], string=link.Title)
        if liHref["href"][0:4] == "http":
            link.update(actions=[
                AwesomeVueLinks.Url.set(liHref["href"])
            ])
        else:
            link.delete()
            print(liHref["href"][1:4])
            # print(liHref)
            # ferror.write("{},{},No Desc\n".format(link.Title, link.Url))

    except:
        ferror.write("{},{},No Li\n".format(link.Title, link.Url))

ferror.close()