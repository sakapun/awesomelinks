import requests
from bs4 import BeautifulSoup as bs
from model.AwesomeVueLinks import AwesomeLinks2



r = requests.get("https://github.com/vuejs/awesome-vue")
page = bs(r.text, features="html.parser")

scan = AwesomeLinks2.scan()
for item in scan:
    a_found = page.find_all(href=item.url)
    print(len(a_found))
    if len(a_found) == 0:
        f = open("../log/delete_log.txt", "a")
        f.write("delete item {}\n".format(item.url))
        f.close()
        print(item.title)
        item.delete()
