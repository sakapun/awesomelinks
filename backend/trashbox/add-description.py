from model.AwesomeVueLinks import AwesomeVueLinks
from bs4 import BeautifulSoup as bs
import re

html = ""
with open("awesome-vue-github.html") as f:
    html = bs(f.read(), features="html.parser")

ferror = open("error-log.csv", mode="w")
ferror.write("Title,Url,Msg\n")

for link in AwesomeVueLinks.query("link", limit=8000, scan_index_forward = False):
    try:
        liTitle = html.find(["strong", "a"], string=link.Title).parent
        liTitle.find("a").decompose()
        description = liTitle.text.strip(" - ")
        if description:
            link.update(actions=[
                AwesomeVueLinks.Description.set(description)
            ])
            print("Description: {} / Title: {}\n".format(description, link.Title))
        else:
            ferror.write("{},{},No Desc\n".format(link.Title, link.Url))

    except:
        ferror.write("{},{},No Li\n".format(link.Title, link.Url))

ferror.close()