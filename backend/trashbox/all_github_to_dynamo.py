import requests
import json
import os
import re
from bs4 import BeautifulSoup as bs
from model.AwesomeVueLinks import AwesomeLinks2



def get_recent_pull_requests(token, start_cursor=""):
    '''
    Github API v4 で最新のプルリクを取得する
    :param start_cursor: string
    :param token: string
    :return: dict {
        title
        merged
        mergedAt
        url
        id
    }
    '''
    url = 'https://api.github.com/graphql'

    beforeQuery = "" if start_cursor == "" else ', before: "{}"'.format(start_cursor)

    queryStr = '''
    query { 
      repository(owner: "vuejs", name: "awesome-vue") { 
        pullRequests(last: #number#, states: [MERGED], orderBy: {field: UPDATED_AT,direction: ASC} #beforeQuery#) {
          nodes {
              title
              merged
              mergedAt
              url
              id
          }
          pageInfo {
            startCursor
            hasPreviousPage
          }
        }
      }
    }
    '''.replace("#number#", "100").replace("#beforeQuery#", beforeQuery)

    query = {"query" : queryStr}

    headers = {"Authorization": "bearer {}".format(token)}
    r = requests.post(url, data=json.dumps(query), headers=headers)

    api_response = json.loads(r.text)
    return api_response["data"]["repository"]["pullRequests"]["nodes"], api_response["data"]["repository"]["pullRequests"]["pageInfo"]



def get_additions(url):
    r = requests.get(url)
    pr_page = bs(r.text)
    added = pr_page.select(".blob-code-addition .blob-code-inner")
    regex = r"\[(\*\*|)(.*?)(\*\*|★.*|)\] ?\((http.*?)\)((.*?([\(<\[\w].*))|)"

    result = []

    for add_line in added:
        add_line_text = add_line.text.replace("\n", "")
        searched = re.search(regex, add_line_text, re.MULTILINE | re.DOTALL)
        if searched:
            result.append({
                "title": searched.group(2),
                "url": searched.group(4),
                "description": searched.group(7)
            })
            # print (searched.group(2))
            #             # print(add_line_text)
            # ""
        else:
            ""
            # print(add_line.text.replace("\n", ""))


    return result



def do_write_connect(new_addition):
    with open("log/output.txt", mode="a") as f:
        try:
            awesome_link = AwesomeLinks2.get(new_addition["url"])
            print("github_at:{}, dynamo_at:{}, all: {}".format(new_addition["mergedAt"], awesome_link.mergedAt, new_addition))
        except:
            link = AwesomeLinks2(
                new_addition["url"],
                title = new_addition["title"],
                type = "link",
                mergedAt = new_addition["mergedAt"],
                description = new_addition["description"]
            ).save()
            f.write("{}\n".format(new_addition))



f = open("log/output.txt", mode="a").close()

endFlg = False
startCursor = "Y3Vyc29yOnYyOpK5MjAxOC0wMS0xMVQwMTo1NzoyNCswOTowMM4Jp3gv"
while not endFlg:
    f = open("log/output.txt", mode="a")
    f.write("--------------------\n\n--------------------\nstartCursor: {}\n".format(startCursor))
    f.close()

    pullRequests, pageInfo = get_recent_pull_requests(os.environ["GITHUB_ACCESS_TOKEN"], start_cursor=startCursor)
    # ページング的な処理
    startCursor = pageInfo["startCursor"]
    endFlg = not pageInfo["hasPreviousPage"]

    pullRequests.reverse()
    for pr in pullRequests:
        diffPage = pr["url"] + "/files"
        additions = get_additions(diffPage)
        for addition in additions:
            addition["mergedAt"] = pr["mergedAt"]
            do_write_connect(addition)


