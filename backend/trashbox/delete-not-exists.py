from model.AwesomeVueLinks import AwesomeVueLinks
from bs4 import BeautifulSoup as bs
import re

html = ""
with open("awesome-vue-github.html") as f:
    html = bs(f.read(), features="html.parser")

fok = open("ok-log-haslink.csv", mode="w")
fok.write("Title,Url\n")

for link in AwesomeVueLinks.scan(AwesomeVueLinks.Description.does_not_exist()):
    liHref = html.find(attrs={"href":link.Url})
    if liHref:
        fok.write("{},{}\n".format(link.Title, link.Url))
    else:
        link.delete()
        print("{}, {}".format(link.Title, link.Url))

fok.close()