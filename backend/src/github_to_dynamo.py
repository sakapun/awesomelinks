import requests
import json
import os
import re
from datetime import datetime
from bs4 import BeautifulSoup as bs
from model.AwesomeVueLinks import AwesomeLinks2



def get_recent_pull_requests(token, limit=20, start_cursor=None):
    '''
    Github API v4 で最新のプルリクを取得する
    :param start_cursor: string
    :param token: string
    :return: dict {
        title
        merged
        mergedAt
        url
        id
    }
    '''
    url = 'https://api.github.com/graphql'
    queryStr = '''
    query Q($limit: Int!, $before: String){ 
      repository(owner: "vuejs", name: "awesome-vue") { 
        pullRequests(last: $limit, states: [MERGED], orderBy: {field: UPDATED_AT,direction: ASC}, before: $before) {
          nodes {
              title
              merged
              mergedAt
              url
              id
          }
          pageInfo {
            startCursor
            hasPreviousPage
          }
        }
      }
    }
    '''
    variables = {
        "limit": limit,
        "before": start_cursor
    }

    query = {"query" : queryStr, "variables": variables}

    headers = {"Authorization": "bearer {}".format(token)}
    r = requests.post(url, data=json.dumps(query), headers=headers)

    api_response = json.loads(r.text)
    return api_response["data"]["repository"]["pullRequests"]["nodes"], api_response["data"]["repository"]["pullRequests"]["pageInfo"]



def get_additions(url):
    r = requests.get(url)
    pr_page = bs(r.text, features="html.parser")
    added = pr_page.select(".blob-code-addition .blob-code-inner")
    regex = r"\[(\*\*|)(.*?)(\*\*|★.*|)\] ?\((http.*?)\)((.*?([\(<\[\w].*))|)"

    result = []

    for add_line in added:
        add_line_text = add_line.text.replace("\n", "")
        searched = re.search(regex, add_line_text, re.MULTILINE | re.DOTALL)
        if searched:
            result.append({
                "title": searched.group(2),
                "url": searched.group(4),
                "description": searched.group(7)
            })
            # print (searched.group(2))
            #             # print(add_line_text)
            # ""
        else:
            ""
            # print(add_line.text.replace("\n", ""))


    return result



def do_write_connect(new_addition):
    operation_type = None
    try:
        awesome_link = AwesomeLinks2.get(new_addition["url"])

        dynamo_at = datetime.strptime(awesome_link.mergedAt, "%Y-%m-%dT%H:%M:%SZ")
        scripted_at =  datetime.strptime(new_addition["mergedAt"], "%Y-%m-%dT%H:%M:%SZ")
        # print(dynamo_at, scripted_at)
        if dynamo_at < scripted_at:
            awesome_link.title = new_addition["title"]
            awesome_link.description = new_addition["description"]
            awesome_link.mergedAt = new_addition["mergedAt"]
            awesome_link.save()
            operation_type = "update"
    except AwesomeLinks2.DoesNotExist:
        AwesomeLinks2(
            new_addition["url"],
            title = new_addition["title"],
            type = "link",
            mergedAt = new_addition["mergedAt"],
            description = new_addition["description"]
        ).save()
        operation_type = "add"

    return operation_type



def notify_admin(addition):
    body = {
        "value1": addition["title"],
        "value2": addition["url"],
        "value3": addition["description"]
    }
    requests.post("https://maker.ifttt.com/trigger/add_awesome_vue/with/key/pywreGClKJxI33Wb16HXm0YrpAakFLgdxtZBB3QqEvy",
                  data=body)
    print(addition)


def notify_twitter(addition):
    body = {
        "value1": addition["title"],
        "value2": addition["url"],
        "value3": addition["description"]
    }
    requests.post("https://maker.ifttt.com/trigger/add_awesome_vue/with/key/pywreGClKJxI33Wb16HXm0YrpAakFLgdxtZBB3QqEvy",
                  data=body)
    print(addition)



pullRequests, _ = get_recent_pull_requests(os.environ["GITHUB_ACCESS_TOKEN"], limit=10)
# pullRequests.reverse()

for pr in pullRequests:
    diffPage = pr["url"] + "/files"
    additions = get_additions(diffPage)
    for addition in additions:
        addition["mergedAt"] = pr["mergedAt"]
        operation_type = do_write_connect(addition)
        if operation_type == "add":
            notify_twitter(addition)
        elif operation_type == "update":
            notify_admin(addition)


