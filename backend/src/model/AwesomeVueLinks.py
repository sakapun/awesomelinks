from pynamodb.attributes import UnicodeAttribute, BooleanAttribute, UTCDateTimeAttribute
from pynamodb.models import Model

class AwesomeVueLinks(Model):
    class Meta:
        table_name = 'AwesomeVueLinks'
        region = 'ap-northeast-1'

    Type = UnicodeAttribute(hash_key=True, null=False)
    MergedAt = UnicodeAttribute(range_key=True, null=False)
    Title = UnicodeAttribute(null=True, default="")
    Url = UnicodeAttribute(null=True, default="")
    Id = UnicodeAttribute(null=False)
    Description = UnicodeAttribute(null=True, default="")



class AwesomeLinks2(Model):
    class Meta:
        table_name = 'AwesomeLinks2'
        region = 'ap-northeast-1'

    title = UnicodeAttribute(null=False)
    url = UnicodeAttribute(hash_key=True)
    description = UnicodeAttribute(null=True, default="")
    mergedAt = UnicodeAttribute(null=False)
    type = UnicodeAttribute(null=False)
