import boto3
import requests
from bs4 import BeautifulSoup as bs

r = requests.get("https://github.com/vuejs/awesome-vue")
awesomeVueGit = bs(r.text, features="html.parser")
with open("awesome-vue-github.html", mode="w") as f:
    f.write(r.text)
# print(awesomeVueGit)